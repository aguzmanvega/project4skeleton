package edu.berkeley.cs162;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class Testing 
{
	public static void main(String[] args) throws KVException, UnknownHostException
	{			
	    Runnable Coord = new Runnable() {
			public void run() { 
				try {
					Server.main(new String[] {"400@127.0.0.1:1500","600@127.0.0.1:1600"});
				} catch (Exception e) {
					e.printStackTrace();
				} 
			}
	    };	
	
	    Runnable SlaveA = new Runnable() {
			public void run() { 
				try {
					SlaveServer.main(new String[] {"400","127.0.0.1","8080" ,"9090"});
				} catch (Exception e) {
					e.printStackTrace();
				} 
			}
	    };		
	    Runnable SlaveB = new Runnable() {
			public void run() { 
				try {
					SlaveServer.main(new String[] {"600","127.0.0.1","8080" ,"9090"});
				} catch (Exception e) {
					e.printStackTrace();
				} 
			}
	    };
		new Thread(Coord).start();
		new Thread(SlaveA).start();
		new Thread(SlaveB).start();
		// First run Server separately (List of SlaveServerID@HostName:Port as arguments)
		// Second run SlaveServer separately (Arguments: <slaveID> <masterHostName[127.0.0.1]> <masterPort [8080]> <registrationPort>)
		// Finally you can run these tests
		TPCMasterTests();
	}
	
	public static void TPCMasterTests() throws KVException, UnknownHostException
	{
		KVClient<String, String> client = new KVClient<String, String>("127.0.0.1", 8080);
		
		client.put("hio", "wtf");
		System.out.println("wee");
		System.out.println(client.get("hio"));
	}

}
