/**
 * Handle TPC connections over a socket interface
 * 
 * @author Mosharaf Chowdhury (http://www.mosharaf.com)
 *
 * Copyright (c) 2012, University of California at Berkeley
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of University of California, Berkeley nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *    
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL PRASHANTH MOHAN BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package edu.berkeley.cs162;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Implements NetworkHandler to handle 2PC operation requests from the Master/
 * Coordinator Server
 *
 */
public class TPCMasterHandler<K extends Serializable, V extends Serializable> implements NetworkHandler {
	private KeyServer<K, V> keyserver = null;
	private ThreadPool threadpool = null;
	private TPCLog<K, V> tpcLog = null;
	private int operationID = 0;

	private ArrayList<KVMessage> requestQueue = null;
	public TPCMasterHandler(KeyServer<K, V> keyserver) {
		this(keyserver, 1);
	}

	public TPCMasterHandler(KeyServer<K, V> keyserver, int connections) {
		this.keyserver = keyserver;
		threadpool = new ThreadPool(connections);	
	}

	@Override
	public void handle(Socket client) throws IOException {
		System.out.println("Here?");
		// implement me
		KVMessage msgIn = null;
	    PrintWriter out = null;
	try	
	{ out = new PrintWriter(client.getOutputStream(), true);
	  msgIn = new KVMessage(client.getInputStream());
	  System.out.println(msgIn.toXML());
		
	  
	  
	  if (msgIn.getmsgType().equals("getreq")){
	    	
	    	
	    	KVMessage response = new KVMessage("resp",msgIn.getKey(),msgIn.getValue(),null,null,null);
			out.println(response.toXML());
			} 
	  
	  
	  
	  
	  
	     if (msgIn.getmsgType().equals("putreq")){
	    	requestQueue.add(Integer.valueOf(msgIn.getTpcOpId()), msgIn);
	    	KVMessage log = new KVMessage("ready", msgIn.getKey(),msgIn.getValue(), null,"putreq",msgIn.getTpcOpId());
		    tpcLog.entries.add(log);
		    KVMessage response = new KVMessage("ready",null,null,null,null,msgIn.getTpcOpId());
			out.println(response.toXML());
			} 
		
	     
	     
	     
	     
			if (msgIn.getmsgType().equals("deltreq")){
				requestQueue.add(Integer.valueOf(msgIn.getTpcOpId()),msgIn);
				String operationID = String.valueOf(this.operationID);
				KVMessage log = new KVMessage("ready", msgIn.getKey(),null, null,"delreq",msgIn.getTpcOpId());
				tpcLog.entries.add(log);
				KVMessage response = new KVMessage("ready",null,null,null,null,msgIn.getTpcOpId());
				out.println(response.toXML());
			} 
			if (msgIn.getmsgType().equals("abort")) {
			      
				KVMessage log = new KVMessage("abort",null,null,null,null,msgIn.getTpcOpId());
			
				tpcLog.entries.add(log);
				KVMessage ack = new KVMessage("abort",null,null,null,"Error Message",msgIn.getTpcOpId());
				out.println(ack.toXML());
			}
								
		    if (msgIn.getmsgType().equals("commit")) {
		    	KVMessage requestMsg = requestQueue.get(Integer.valueOf(msgIn.getTpcOpId()));
		    	if (requestMsg.getmsgType().equals("putreq")){
		    		keyserver.put((K)KVMessage.decodeObject(msgIn.getKey()), (V)KVMessage.decodeObject(msgIn.getValue()));
		    	}
		    	
		    	
		    	else {
		    		keyserver.del((K)KVMessage.decodeObject(msgIn.getKey()));
		    	}
		    	
		    	
			
				KVMessage log = new KVMessage("commit",null,null,null,null,msgIn.getTpcOpId());
				tpcLog.entries.add(log);
				KVMessage ack = new KVMessage("ack",null,null,null,null,msgIn.getTpcOpId());
				out.println(ack.toXML());
				}
			
		} 
     	catch (KVException e) {
			out.println(e.getMsg().toXML());
		} catch (IOException e) {
	}	
}


	

	
	/**
	 * Set TPCLog after it has been rebuilt
	 * @param tpcLog
	 */
	public void setTPCLog(TPCLog<K, V> tpcLog) {
		this.tpcLog  = tpcLog;
	}

}
