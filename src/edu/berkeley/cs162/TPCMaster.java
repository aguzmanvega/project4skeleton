/**
 * Master for Two-Phase Commits
 * 
 * @author Mosharaf Chowdhury (http://www.mosharaf.com)
 *
 * Copyright (c) 2012, University of California at Berkeley
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of University of California, Berkeley nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *    
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL PRASHANTH MOHAN BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package edu.berkeley.cs162;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.locks.*;

public class TPCMaster<K extends Serializable, V extends Serializable>  {
	
	// Timeout value used during 2PC operations
	private static final int TIMEOUT_MILLISECONDS = 5000;

	// Cache stored in the Master/Coordinator Server
	private KVCache<K, V> masterCache = new KVCache<K, V>(1000);

	// Registration server that uses TPCRegistrationHandler
	public SocketServer regServer = null;

	// ID of the next 2PC operation
	private Long tpcOpId = 0L;

	static public ArrayList<KVMessage> tpcResponse = new ArrayList<KVMessage>();
	private ReentrantReadWriteLock cacherw = new ReentrantReadWriteLock();
	private ReentrantLock tpcLock = new ReentrantLock();
	private Condition cLock = tpcLock.newCondition();
	boolean update = false;
	ArrayList<K> keysInUse = new ArrayList<K>();
	ArrayList<SlaveInfo> slaveList = new ArrayList<SlaveInfo>();
	TPCRegistrationHandler handler;

	/**
	 * Implements NetworkHandler to handle registration requests from 
	 * SlaveServers.
	 * 
	 */
	private class TPCRegistrationHandler implements NetworkHandler{

		private ThreadPool threadpool = null;


		public TPCRegistrationHandler() {
			// Call the other constructor
			this(1);	
		}

		public TPCRegistrationHandler(int connections) {
			threadpool = new ThreadPool(connections);	
		}

		public class regDealer implements Runnable{

			Socket regClient = null;

			public regDealer(Socket client){
				this.regClient = client;
			}

			public void run() {
				// for later
				KVMessage msgIn = null;
				try {
					while(regClient.getInputStream().available() < 1)
					{
						Thread.sleep(50);
					}
					System.out.println(regClient.getInputStream().available());
					msgIn = new KVMessage(regClient.getInputStream());
				} catch (KVException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


				String type =  msgIn.getmsgType() ;
				if (type.equals("register"))
				{
					SlaveInfo slave = null;
					try {
						slave = new SlaveInfo(msgIn.getMessage());
						slave.setKvSocket(regClient);
					} catch (KVException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					slaveList.add(slave);
					PrintWriter out = null;
					try {
						out = new PrintWriter(regClient.getOutputStream(), true);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}		
					KVMessage regAck = new KVMessage("resp", "Successfully registered "+ slave.slaveID+"@" +slave.hostName+":"+slave.port);				
					
					out.println(regAck.toXML());
//					out.close();
//					try {
//						regClient.close();
//					} catch (IOException e) {
//						e.printStackTrace();
//					}
				}
			}
		}





		@Override
		public void handle(Socket client) throws IOException {
			// implement me
			Runnable r = new regDealer(client);
			try {
				threadpool.addToQueue(r);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 *  Data structure to maintain information about SlaveServers
	 *
	 */
	private class SlaveInfo {
		// 64-bit globally unique ID of the SlaveServer
		private long slaveID = -1;
		// Name of the host this SlaveServer is running on
		private String hostName = null;
		// Port which SlaveServer is listening to
		private int port = -1;

		// Variables to be used to maintain connection with this SlaveServer
		private KVClient<K, V> kvClient = null;
		private Socket kvSocket = null;

		/**
		 * 
		 * @param slaveInfo as "SlaveServerID@HostName:Port"
		 * @throws KVException
		 * @throws IOException 
		 */
		public SlaveInfo(String slaveInfo) throws KVException, IOException {
			// implement me
			String delimiter1 = "@";
			String delimiter2 = ":";
			String id = slaveInfo.split(delimiter1)[0];
			slaveID = Long.valueOf(id);
			String rest = slaveInfo.split(delimiter1)[1];
			hostName = rest.split(delimiter2)[0];
			port = Integer.valueOf(rest.split(delimiter2)[1]);
		}

		public long getSlaveID() {
			return slaveID;
		}

		public KVClient<K, V> getKvClient() {
			return kvClient;
		}

		public Socket getKvSocket() {
			return kvSocket;
		}

		public void setKvSocket(Socket kvSocket) {
			this.kvSocket = kvSocket;
		}
	}


	public class KVCommit extends Thread {

		private KVMessage msg = null;
		int index=0;
		SlaveInfo slave = null;
		String type = null;

		public KVCommit(KVMessage msg, int i, SlaveInfo slave) {
			this.msg = msg;
			index = i;
			this.slave = slave;
		}

		public KVCommit(String string, int i, SlaveInfo slave) {
			this.type = string;
			index = i;
			this.slave = slave;
		}

		@Override
		public void run() {
			PrintWriter out = null ;
			if (msg != null) { //Prepare Message
				try {
						 slave.setKvSocket(new Socket(slave.hostName, slave.port));
						tpcResponse.add(index, new KVMessage("timeout", null)); 
						out = new PrintWriter(slave.kvSocket.getOutputStream(), true);
						if (msg.getmsgType().equals("putreq")) {
							out.println((new KVMessage("putreq", msg.getKey(), msg.getValue())).toXML());
						} else if (msg.getmsgType().equals("delreq")) {
							out.println((new KVMessage("delreq", msg.getKey(), msg.getValue())).toXML());
						} else {}
						out.println((new KVMessage("putreq", msg.getKey(), msg.getValue())).toXML());
						slave.kvSocket.setSoTimeout(TIMEOUT_MILLISECONDS);
						tpcResponse.add(index, new KVMessage(slave.kvSocket.getInputStream())); 
					
				} catch (IOException e) {
				} catch (KVException e) {
					tpcResponse.add(index, e.getMsg()); //This could be a timeout exception or some other type of exception (unparsable error).
				} finally {
					//try {
						//slave.kvSocket.setSoTimeout(TIMEOUT_MILLISECONDS);
					//} catch (SocketException e) {}
				}
			} else { //subsequent requests
				while (true) {
					try {
						if (!slave.getKvSocket().isConnected()) {
							slave.getKvSocket().connect((SocketAddress) new InetSocketAddress(slave.hostName, slave.port));
						} else {
							out = new PrintWriter(slave.kvSocket.getOutputStream(), true);
							if (type.equals("commit")) {
								out.println((new KVMessage("commit", null)).toXML());
							} else if (type.equals("abort")) {
								out.println((new KVMessage("abort", null)).toXML());
							}
							slave.kvSocket.setSoTimeout(TIMEOUT_MILLISECONDS);
							tpcResponse.add(new KVMessage(slave.kvSocket.getInputStream())); 
							slave.kvSocket.setSoTimeout(0);
							break;
						}
					} catch (IOException e) {
					} catch (KVException e) {	
					}
				}

			}
		}
	}

	/**
	 * Creates TPCMaster using SlaveInfo provided as arguments and SlaveServers 
	 * actually register to let TPCMaster know their presence
	 * 
	 * @param listOfSlaves list of SlaveServers in "SlaveServerID@HostName:Port" format
	 * @throws Exception
	 */

	public void setRegServer(SocketServer reg){
		regServer = reg;
	}

	public TPCMaster(String[] listOfSlaves) throws Exception {
		
		for (int i =0;i<listOfSlaves.length;i++){
			slaveList.add(new SlaveInfo(listOfSlaves[i]));
		}
		// Create registration server
		
		regServer = new SocketServer("127.0.0.1", 9090);
        handler = new TPCRegistrationHandler();
        regServer.addHandler(handler);
        regServer.connect();
	}

	/**
	 * Calculates tpcOpId to be used for an operation. In this implementation
	 * it is a long variable that increases by one for each 2PC operation. 
	 * 
	 * @return 
	 */
	private String getNextTpcOpId() {
		tpcOpId++;
		return tpcOpId.toString();		
	}

	/**
	 * Start registration server in a separate thread
	 */
	public void run() {
        new Thread(new Runnable() {
                public void run() {
                        try {
                                regServer.run();
                        } catch (IOException e) {
                                e.printStackTrace();
                        }
                }
        }).start();}
	/**
	 * Converts Strings to 64-bit longs
	 * Borrowed from http://stackoverflow.com/questions/1660501/what-is-a-good-64bit-hash-function-in-java-for-textual-strings
	 * Adapted from String.hashCode()
	 * @param string String to hash to 64-bit
	 * @return
	 */
	private long hashTo64bit(String string) {
		// Take a large prime
		long h = 1125899906842597L; 
		int len = string.length();

		for (int i = 0; i < len; i++) {
			h = 31*h + string.charAt(i);
		}
		return h;
	}

	/**
	 * Compares two longs as if they were unsigned (Java doesn't have unsigned data types except for char)
	 * Borrowed from http://www.javamex.com/java_equivalents/unsigned_arithmetic.shtml
	 * @param n1 First long
	 * @param n2 Second long
	 * @return is unsigned n1 less than unsigned n2
	 */
	private boolean isLessThanUnsigned(long n1, long n2) {
		return (n1 < n2) ^ ((n1 < 0) != (n2 < 0));
	}

	private boolean isLessThanEqualUnsigned(long n1, long n2) {
		return isLessThanUnsigned(n1, n2) || n1 == n2;
	}	

	/**
	 * Find first/primary replica location
	 * @param key
	 * @return
	 */
	private SlaveInfo findFirstReplica(K key) {
		// 64-bit hash of the key
		long hashedKey = hashTo64bit(key.toString());

		long min = Long.MAX_VALUE;

		SlaveInfo temp = slaveList.get(0);

		for(int i = 0; i < slaveList.size(); i++)
		{
			if(hashedKey < slaveList.get(i).getSlaveID())
				if(slaveList.get(i).getSlaveID() < min)
				{
					min = slaveList.get(i).getSlaveID();
					temp = slaveList.get(i);
				}
		}
		return temp;
	}

	/**
	 * Find the successor of firstReplica to put the second replica
	 * @param firstReplica
	 * @return
	 */
	private SlaveInfo findSuccessor(SlaveInfo firstReplica) {
		int num = 0;

		for(int i = 0; i < slaveList.size(); i++)
		{
			if(firstReplica.getSlaveID() == slaveList.get(i).getSlaveID())
			{
				num = i;
			}
		}
		
		System.out.println(num);

		if(num < slaveList.size() - 1)
			return slaveList.get(num + 1);
		else
			return slaveList.get(0);
	}

	/**
	 * Synchronized method to perform 2PC operations one after another
	 * 
	 * @param msg
	 * @param isPutReq
	 * @return True if the TPC operation has succeeded
	 * @throws KVException
	 */
	@SuppressWarnings("unchecked")
	public synchronized boolean performTPCOperation(KVMessage msg, boolean isPutReq) throws KVException {
		boolean aborted = false;
		String errorReason1 = "";
		String errorReason2 = "";
		String errorReasonConcat = "";
		K decodedKey = (K) KVMessage.decodeObject(msg.getKey());
		V decodedValue = (V) KVMessage.decodeObject(msg.getValue());
		SlaveInfo firstSlaveInfo = findFirstReplica(decodedKey);
		
		System.out.println(firstSlaveInfo.hostName);
		SlaveInfo secondSlaveInfo=findSuccessor(firstSlaveInfo);
		//Lock to access update and change KeysInUse
		tpcLock.lock();
		while (keysInUse.contains(decodedKey))
		{
			try {
				cLock.await();
			} catch (InterruptedException e) {}
		}
		update = true;
		keysInUse.add(decodedKey);
		tpcLock.unlock();
		//Send prepare request put/del and wait for response ready/abort
		KVCommit KVC1 = new KVCommit(msg,0,firstSlaveInfo);
		KVCommit KVC2 = new KVCommit(msg,1,secondSlaveInfo);
		KVC1.start();
		KVC2.start();
		try {
			KVC1.join();
			KVC2.join();
		} catch (InterruptedException e) {}
		//decide what kind of message to send back
		if (tpcResponse.get(0).getMessage().equals("ready") && tpcResponse.get(1).getMessage().equals("ready")) {
			KVC1 = new KVCommit("commit",0,firstSlaveInfo);
			KVC2 = new KVCommit("commit",1,secondSlaveInfo);
		} else {			
			aborted = true;
			if (tpcResponse.get(0).getmsgType().equals("timeout")) {
				errorReason1 = errorReason1 + "Timeout Error: SlaveServer " + firstSlaveInfo.slaveID + " has timed out during the first phase of 2PC";
			} else if (tpcResponse.get(0).getmsgType().equals("abort")) {
				errorReason1 = errorReason1 + "@" + firstSlaveInfo.slaveID + "=>" + tpcResponse.get(0).getMessage();
			}
			if (tpcResponse.get(1).getmsgType().equals("timeout")) {
				errorReason2 = errorReason2 + "Timeout Error: SlaveServer " + secondSlaveInfo.slaveID + " has timed out during the first phase of 2PC";
			} else if (tpcResponse.get(1).getmsgType().equals("abort")) {
				errorReason2 = errorReason2 + "@" + secondSlaveInfo.slaveID + "=>" + tpcResponse.get(0).getMessage();
			}
			if (!errorReason1.isEmpty()) {
				errorReasonConcat=errorReason1;
				if (!errorReason2.isEmpty()) {
					errorReasonConcat = errorReasonConcat + "\n" + errorReason2;
				}
			} else {
				if (!errorReason2.isEmpty()) {
					errorReasonConcat=errorReason2;
				}
			}
			KVC1 = new KVCommit("abort",0,firstSlaveInfo);
			KVC2 = new KVCommit("abort",1,secondSlaveInfo);
		}
		//Send commit/abort and wait for Ack response
		KVC1.start();
		KVC2.start();
		try {
			KVC1.join();
			KVC2.join();
		} catch (InterruptedException e) {}
		//Lock to access update and change KeysInUse
		tpcLock.lock();
		update = false;
		keysInUse.remove(decodedKey);
		cLock.signalAll();
		tpcLock.unlock();
		// 
		if (tpcResponse.get(0).getMessage().equals("Ack") && tpcResponse.get(1).getMessage().equals("Ack")) {
			if (aborted) {	
				throw new KVException(new KVMessage("resp", errorReasonConcat));
			}
			cacherw.writeLock().lock();
			if (isPutReq) { 
				masterCache.put(decodedKey,decodedValue);
			} else { 
				masterCache.del(decodedKey);
			}
			cacherw.writeLock().unlock();
			return true;
		} else {
			assert false; 
			return false;
		}
	}

	/**
	 * Perform GET operation in the following manner:
	 * - Try to GET from first/primary replica
	 * - If primary succeeded, return Value
	 * - If primary failed, try to GET from the other replica
	 * - If secondary succeeded, return Value
	 * - If secondary failed, return KVExceptions from both replicas
	 * 
	 * @param msg Message containing Key to get
	 * @return Value corresponding to the Key
	 * @throws KVException
	 */
	@SuppressWarnings("unchecked")
	public V handleGet(KVMessage msg) throws KVException {
		KVMessage returnMsgFirst = null;
		KVMessage returnMsgSecond = null;
		KVMessage returnMsgFinal = null;
		PrintWriter out=null;
		KVMessage getVal = new KVMessage("getreq",msg.getKey(),null);
		K decodedKey = (K) KVMessage.decodeObject(msg.getKey());
		//Lock to access update and change KeysInUse
		tpcLock.lock();
		while (update && keysInUse.contains(decodedKey)) {
			try {
				cLock.await();
			} catch (InterruptedException e1) {}
		}
		keysInUse.add(decodedKey);
		tpcLock.unlock();
		//Check to see if value is in masterCache
		if (masterCache.get(decodedKey) != null){ 
			//Lock to change masterCache
			cacherw.readLock().lock();
			V returnVal= masterCache.get(decodedKey);
			cacherw.readLock().unlock();
			//Lock to access update and change KeysInUse
			tpcLock.lock();
			cLock.signalAll();
			keysInUse.remove(decodedKey);
			tpcLock.unlock();
			return returnVal;			
		}
		//Determine first and second slave info
		SlaveInfo firstSlaveInfo = findFirstReplica(decodedKey);
		SlaveInfo secondSlaveInfo=findSuccessor(firstSlaveInfo);
		//Attempt to second to first server
		try {
			out = new PrintWriter(firstSlaveInfo.kvSocket.getOutputStream(), true);
			out.println(getVal.toXML());
			returnMsgFirst = new KVMessage(firstSlaveInfo.kvSocket.getInputStream());
		} catch (KVException e) {
			returnMsgFirst = e.getMsg();
		} catch (SocketException e1) {
		} catch (IOException e) {
		}
		//If first failed try second else send success
		if (returnMsgFirst.getKey() == null) {//if there was a get failure a KVException is thrown with msg type "Unsuccessful Get/Put/Delete Response" which has no key value;
			try {
				out = new PrintWriter(secondSlaveInfo.kvSocket.getOutputStream(), true);				
				out.println(getVal.toXML());
				returnMsgSecond = new KVMessage(secondSlaveInfo.kvSocket.getInputStream());
				returnMsgFinal = returnMsgSecond;
			} catch (KVException e) { //both first and second failed so reset locks appropriately and send KVException.
				returnMsgSecond = e.getMsg();
				tpcLock.lock();
				cLock.signalAll();
				keysInUse.remove(decodedKey);
				tpcLock.unlock();
				throw new KVException(new KVMessage("resp","@SlaveServerID1=>" + returnMsgFirst.getMessage() + "\n" + "@SlaveServerID2=>" + returnMsgSecond.getMessage()));
			} catch (SocketException e1) {
			} catch (IOException e) {
			}
		} else { //If first succeeded
			returnMsgFinal = returnMsgSecond;
		}
		cacherw.writeLock().lock();
		masterCache.put(decodedKey, (V) KVMessage.decodeObject(returnMsgFinal.getValue()));
		cacherw.writeLock().unlock();
		//Unlock Critical Section
		tpcLock.lock();
		cLock.signalAll();
		keysInUse.remove(decodedKey);
		tpcLock.unlock();
		return (V) KVMessage.decodeObject(returnMsgFinal.getValue());
	}
}
