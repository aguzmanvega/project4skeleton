/**
 * Implementation of an LRU Cache (copied from the Internet)
 * 
 * Copyright (c) 2011, University of California at Berkeley
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of University of California, Berkeley nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *    
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL PRASHANTH MOHAN BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package edu.berkeley.cs162;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.locks.ReentrantLock;

/**
 * An LRU cache which has a fixed maximum number of elements (cacheSize).
 * If the cache is full and another entry is added, the LRU (least recently used) entry is dropped.
 */
public class KVCache<K extends Serializable, V extends Serializable> implements KeyValueInterface<K, V>{
	private int cacheSize;
	private LinkedList<K> lruList;
	private HashMap<K, V> cache;
	ReentrantLock readlockLruUpdate = new ReentrantLock(); //Lock necessary because multiple gets can be in the cache at the same time.
    														//Since a get 
	int debug = 0;
	long requests = 0;
	long hits = 0;
	
	
	/**
	 * Creates a new LRU cache.
	 * @param cacheSize the maximum number of entries that will be kept in this cache.
	 */
	public KVCache (int cacheSize) {
		lruList = new LinkedList<K>();
		cache = new HashMap<K, V>(cacheSize);
		this.cacheSize = cacheSize;
		if (debug == 1) System.out.println("Initialized KVCache. Cache size is: " + cacheSize + " Thread: " + Thread.currentThread().getName());
	}

	/**
	 * Retrieves an entry from the cache.
	 * The retrieved entry becomes the MRU (most recently used) entry.
	 * @param key the key whose associated value is to be returned.
	 * @return the value associated to this key, or null if no value with this key exists in the cache.
	 */
	public V get (K key) {
		requests++;
		if (debug == 1) System.out.println("GET cache request for key: " + key + ". Current hit rate: " + getHitRate() + "%. Thread: " + Thread.currentThread().getName());
		if(!cache.containsKey(key)) {
			if (debug == 1) System.out.println("Key: " + key + " is not in cache. Thread: " + Thread.currentThread().getName());
			return null;
		}
		if (debug == 1) System.out.println("Key: " + key + " is in cache. Thread: " + Thread.currentThread().getName());
		hits++;
		readlockLruUpdate.lock();
		lruList.remove(key);
		lruList.addFirst(key);
		readlockLruUpdate.unlock();
		return cache.get(key);
	}

	/**
	 * Adds an entry to this cache.
	 * The new entry becomes the MRU (most recently used) entry.
	 * If an entry with the specified key already exists in the cache, it is replaced by the new entry.
	 * If the cache is full, the LRU (least recently used) entry is removed from the cache.
	 * @param key    the key with which the specified value is to be associated.
	 * @param value  a value to be associated with the specified key.
	 * @return 
	 */
	public boolean put (K key, V value) {
		if (debug == 1) 
			System.out.println("PUT cache request for key: " + key + " Thread: " + Thread.currentThread().getName());
		boolean keyExists = lruList.remove(key);
		if (keyExists) {
			if (debug == 1) 
				System.out.println("Key: " + key + " already exists in cache. Thread: " + Thread.currentThread().getName());
			lruList.addFirst(key);
			cache.put(key, value);
			return true;
		}
		if (debug == 1) 
			System.out.println("Key: " + key + " does not exist in cache so adding it to cache. Thread: " + Thread.currentThread().getName());
		if (lruList.size() >= cacheSize) {
			if (debug == 1) 
				System.out.println("Cache is full so removing a key to add key: " + key + ". Thread: " + Thread.currentThread().getName());
			K removedKey = lruList.getLast();
			lruList.remove(removedKey);
			cache.remove(removedKey);
		}
		lruList.addFirst(key);
		cache.put(key, value);	
		return false;
	}

	/**
	 * Removes an entry to this cache.
	 * @param key the key with which the specified value is to be associated.
	 */
	public void del (K key) {
		if (debug == 1) 
			System.out.println("DEL cache request for key: " + key + " Thread: " + Thread.currentThread().getName());
		if (lruList.remove(key)) {
			if (debug == 1) 
				System.out.println("Key: " + key + " deleted from cache. Thread: " + Thread.currentThread().getName());
			cache.remove(key);
			readlockLruUpdate.unlock();
			return;
		}
		if (debug == 1) 
			System.out.println("Key: " + key + " is not in cache so DEL is N/A. Thread: " + Thread.currentThread().getName());
	}
	
	public long getHitRate() {
		return (100 * (requests-hits))/requests;
	}
} // end class LRUCache
