/**
 * Client-side encryption using triple DES encryption (DES-EDE)
 * 
 * @author Karthik Reddy Vadde
 *
 * Copyright (c) 2011, University of California at Berkeley
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of University of California, Berkeley nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *    
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL PRASHANTH MOHAN BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package edu.berkeley.cs162;

import javax.crypto.Cipher;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.security.InvalidKeyException;

public class KVCrypt {
    private static String algorithm = "DESede";
    private static SecretKey key = null;
    public static String keyStr = null;
    private static Cipher cipher = null; 

    public void setUp() throws Exception {
   		byte[] keyByte = key.getEncoded();  //Encrypts our key 
   		keyStr = keyByte.toString();
    	// implement me
    }

    public void setKey(SecretKey keyPar) throws Exception {
    	key = keyPar;
    }

    public void setCipher() throws Exception {
    	cipher = Cipher.getInstance(algorithm);
    }
    
    public void testEncrypt() throws Exception {
    	cipher.init(Cipher.ENCRYPT_MODE, key);
    	String testW = "Hello";
    	byte[] test1 = encrypt(testW);
    	
    	String testX = "Hello"; 
    	byte[] test2 = encrypt(testX);
    	
    	String testY = "Bye"; 
    	byte[] test3 = encrypt(testY);
    	
    	String decryptedWord1 = decrypt(test1);
    	String decryptedWord2 = decrypt(test2);
    	String decryptedWord3 = decrypt(test3);
    	System.out.println("Should print the same thing. ");
    	System.out.println(decryptedWord1);
    	System.out.println(decryptedWord2);
    	System.out.println("Should be different. ");
    	System.out.println(decryptedWord3);
    
    } 
    
    public byte[] encrypt(String input)
        throws InvalidKeyException, 
               BadPaddingException,
               IllegalBlockSizeException {
    	
    	cipher.init(Cipher.ENCRYPT_MODE, key); //Sets our cipher to incryption mode
    	try{
    		
    		byte[] utf8 = input.getBytes();
    		byte[] encrypted = cipher.doFinal(utf8);
    		return encrypted;
    	}catch(Exception ignored){		
    	
    	}
    	
    	// implement me
    	return null;
    }

    public String decrypt(byte[] encryptionBytes)
        throws InvalidKeyException, 
               BadPaddingException,
               IllegalBlockSizeException {
    	
    	cipher.init(Cipher.DECRYPT_MODE, key); //Sets cypher to decrypt mode
    	try{
    		byte[] dec = cipher.doFinal(encryptionBytes); //Decrypts the inputBytes
    		String str = new String(dec);
    		return str;
    	}catch(Exception ignored){
		
    	}
    	// implement me
    	return null;
      }
}

